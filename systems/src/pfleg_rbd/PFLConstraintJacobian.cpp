/*
 * PFLConstraintJacobian.cpp
 *
 *  Created on: August 4, 2017
 *      Author: depardo
 */

#include <dynamical_systems/systems/pointFootLeg/PFLConstraintJacobian.hpp>

PFLConstraintJacobian::PFLConstraintJacobian(const ContactConfiguration_t & fc):
    Jc_(EEJacobian_t::Zero()),
    feet_configuration_(fc) {

  setJSize(feet_configuration_);
}

PFLConstraintJacobian::PFLConstraintJacobian(const bool &fc):
    Jc_(EEJacobian_t::Zero()),
    feet_configuration_(fc) {

    setJSize(feet_configuration_);
}

void PFLConstraintJacobian::setJSize(const ContactConfiguration_t & ee_conf) {
  j_size_ = 0;
  std::vector<bool> v = ee_conf.ToVector();
  for(auto ee:v) {
    if(ee) j_size_=j_size_+3;
  }
}

void PFLConstraintJacobian::GetCurrentFootJacobian(const size_t & leg , SingleEEJacobian_t & requested_jacobian) {

	requested_jacobian.block<this->kSingleEEContactForceSize,this->kBaseDof>(0,0) = Jb_LF;
	requested_jacobian.block<this->kSingleEEContactForceSize,this->kEEJoints>(0,this->kBaseDof) = Jr_LF;
}

void PFLConstraintJacobian::GetCurrentFeetJacobian(EESingleJacobianDataMap_t & feet_jacobians) {

  for(int l=0; l < this->kTotalEE ; l++) {
      GetCurrentFootJacobian(l,feet_jacobians[l]);
  }
}

void PFLConstraintJacobian::update(const GeneralizedCoordinates_t & big_q) {

  updateJacobians(big_q);
  SetCurrentJc();
}

void PFLConstraintJacobian::updateQuick(const JointCoordinates_t & joints_q_r ,
    const EEPositionsDataMap_t & feet_pose_base) {

  updateJacobiansQuick(joints_q_r,feet_pose_base);
  SetCurrentJc();
}

void PFLConstraintJacobian::GetContactConfigurationJacobian(const GeneralizedCoordinates_t & big_q,
    EEJacobian_t & Jc) {
  update(big_q);
  this->GetJc(Jc);
}

void PFLConstraintJacobian::SetCurrentJc() {

	Jc_.setZero();

	for(int leg = 0; leg < this->kTotalEE ; leg ++) {

	    if(feet_configuration_[leg]) {

        int jac_r_col = this->kBaseDof + ( leg * this->kEEJoints );
        int jac_row = this->kSingleEEContactForceSize*leg;
        switch(leg) {

        case 0:
          Jc_.block<3,6>(jac_row,0) = Jb_LF;
          Jc_.block<3,3>(jac_row,jac_r_col) = Jr_LF;
          break;

        default:
          std::cout << "This should never happen" << std::endl;
          std::exit(EXIT_FAILURE);
        }
		}
	}
}

void PFLConstraintJacobian::ChangeFeetConfiguration(const ContactConfiguration_t & fc) {
  feet_configuration_ = fc;
  setJSize(feet_configuration_);
}

void PFLConstraintJacobian::GetInverse(EEJacobianTranspose_t & JcInv, const double & tolerance) {

  if(j_size_ == 0){
      JcInv.setZero();
      return;
  }
  JcInv = this->psdInv(Jc_, tolerance);
  return;
}

void PFLConstraintJacobian::GetInverse(EEJacobianTranspose_t & JcInv,
                                       const double & tolerance,
                                       Eigen::Matrix<double,kContactForcesSize,kContactForcesSize> & U,
                                       Eigen::Matrix<double, kTotalDof, kTotalDof> & V){
  if(j_size_ == 0){
      JcInv.setZero();
      return;
  }
  EEJacobian_t S;
  JcInv = this->psdInv(Jc_,tolerance,U,S,V);
}
