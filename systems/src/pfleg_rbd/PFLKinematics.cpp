
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>


PFLKinematics::PFLKinematics():homogeneousTransforms_(),
legJacobians_() { }

double PFLKinematics::getDefaultGroundDistance() {

  GeneralizedCoordinates_t big_q;
  GetCanonicalPose(big_q);
  EEPositionsDataMap_t base_feet;
  GetFeetPoseBase(big_q,base_feet);
  return (base_feet[0](2));
}

void PFLKinematics::GetFeetPose(const GeneralizedCoordinates_t & big_q , EEPositionsDataMap_t & feet_pose_inertia ,
                                EEPositionsDataMap_t & feet_pose_base) {

  BaseCoordinates_t base_pose;
  Eigen::Affine3d base_transformation;

  GetFloatingBaseCoordinates(big_q, base_pose);
  GetWBTransform(base_pose, base_transformation);
  GetFeetPoseBase(big_q,feet_pose_base);
  GetFeetPoseInertia(base_transformation,feet_pose_base,feet_pose_inertia);
}

void PFLKinematics::GetFeetPoseBase(const GeneralizedCoordinates_t & big_q , EEPositionsDataMap_t & p_base) {

  JointCoordinates_t qr;
  GetActuatedCoordinates(big_q,qr);
  GetFeetPoseBaseFromJoints(qr, p_base);
}

void PFLKinematics::GetFeetPoseBaseFromJoints(const JointCoordinates_t & qr , EEPositionsDataMap_t & p_base) {

     p_base[0] = homogeneousTransforms_.fr_trunk_X_LF_foot(qr).block<3,1>(0,3);
}

void PFLKinematics::GetRealFeetVelocity(const JointCoordinates_t & qr , const JointCoordinates_t & qrd ,
                                        EEPositionsDataMap_t & f_vel) {

    f_vel[0] = legJacobians_.fr_trunk_J_LF_foot.update(qr).block<3,3>(iit::rbd::LX,0) * qrd.segment<3>(0);
}

void PFLKinematics::GetCanonicalPose(GeneralizedCoordinates_t & big_q) {

  //ToDo : Check the value of the foot position in world coordinates given this canonical position.
  big_q(0) =    0.0 ;
  big_q(1) =    0.0 ;
  big_q(2) =    0.0 ;

  big_q(3) =    0.0 ;
  big_q(4) =    0.0 ;
  big_q(5) =    0.0 ;

  big_q(6) =    0.0;
  big_q(7) =    0.7;
  big_q(8) =    -1.4;
}

void PFLKinematics::GetCanonicalPoseState(state_vector_t & x_canon) {

  GeneralizedCoordinates_t q,qd;
  GetCanonicalPose(q);
  qd.setZero();
  StateVectorFromGeneralizedCoordinates(q,qd,x_canon);
}
