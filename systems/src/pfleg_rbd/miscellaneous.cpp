#include <iit/rbd/utils.h>
#include <dynamical_systems/systems/pointFootLeg/iit/miscellaneous.h>

using namespace iit::pfLeg;
using namespace iit::pfLeg::dyn;

iit::rbd::Vector3d iit::pfLeg::getWholeBodyCOM(
    const InertiaProperties& inertiaProps,
    const HomogeneousTransforms& ht)
{
    iit::rbd::Vector3d tmpSum(iit::rbd::Vector3d::Zero());

    tmpSum += inertiaProps.getCOM_trunk() * inertiaProps.getMass_trunk();

    HomogeneousTransforms::MatrixType tmpX(HomogeneousTransforms::MatrixType::Identity());
    tmpX = tmpX * ht.fr_trunk_X_fr_LF_hipassembly;
    tmpSum += inertiaProps.getMass_LF_hipassembly() *
            ( iit::rbd::Utils::transform(tmpX, inertiaProps.getCOM_LF_hipassembly()));
    
    tmpX = tmpX * ht.fr_LF_hipassembly_X_fr_LF_upperleg;
    tmpSum += inertiaProps.getMass_LF_upperleg() *
            ( iit::rbd::Utils::transform(tmpX, inertiaProps.getCOM_LF_upperleg()));
    
    tmpX = tmpX * ht.fr_LF_upperleg_X_fr_LF_lowerleg;
    tmpSum += inertiaProps.getMass_LF_lowerleg() *
            ( iit::rbd::Utils::transform(tmpX, inertiaProps.getCOM_LF_lowerleg()));
    

    return tmpSum / inertiaProps.getTotalMass();
}

iit::rbd::Vector3d iit::pfLeg::getWholeBodyCOM(
    const InertiaProperties& inertiaProps,
    const JointState& q,
    HomogeneousTransforms& ht)
{
    // First updates the coordinate transforms that will be used by the routine
    ht.fr_trunk_X_fr_LF_hipassembly(q);
    ht.fr_LF_hipassembly_X_fr_LF_upperleg(q);
    ht.fr_LF_upperleg_X_fr_LF_lowerleg(q);

    // The actual calculus
    return getWholeBodyCOM(inertiaProps, ht);
}
