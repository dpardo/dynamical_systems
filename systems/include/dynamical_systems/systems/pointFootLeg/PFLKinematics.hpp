/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * PFLKinematics.hpp
 *
 * by : Diego Pardo
 * date : 03/08/2017
 *
 */

#ifndef PFLKINEMATICS_HPP_
#define PFLKINEMATICS_HPP_

#include <memory>
#include <ctime>
#include <Eigen/Dense>

#include <dynamical_systems/systems/pointFootLeg/iit/jacobians.h>
#include <dynamical_systems/systems/pointFootLeg/iit/transforms.h>
#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/tools/LRConversions.hpp>

/**
 * @brief This class describes the Kinematics of a Single Point Foot Legged Robot
 */
class PFLKinematics : public LRConversions<robotDimensions> {

public :

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef robotDimensions::ContactConfiguration_t ContactConfiguration_t;
	typedef robotDimensions::EEPositionsDataMap_t EEPositionsDataMap_t;
	typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef robotDimensions::BaseCoordinates_t BaseCoordinates_t;
  typedef robotDimensions::JointCoordinates_t JointCoordinates_t;
	typedef robotDimensions::Vector6D_t Vector6D_t;
	typedef robotDimensions::Matrix66D_t Matrix66D_t;
	typedef robotDimensions::EEJacobianTranspose_t EEJacobianTranspose_t;
	typedef robotDimensions::InertiaMatrix_t InertiaMatrix_t;
	typedef robotDimensions::state_vector_t state_vector_t;

	/**
	 * @brief The constructor
	 */
	PFLKinematics();
  ~PFLKinematics(){}

	iit::pfLeg::HomogeneousTransforms homogeneousTransforms_;
	iit::pfLeg::Jacobians legJacobians_;

	/**
	 * @brief the default configuration of the robot
	 * @param big_q the GeneralizedCoordinate vector with the default configuration of the robot
	 */
	virtual void GetCanonicalPose(GeneralizedCoordinates_t & big_q);

	/**
	 * @brief the default configuration of the robot
	 * @param x_canon the StateVector with the default configuration of the robot
	 */
  void GetCanonicalPoseState(state_vector_t & x_canon);

  /**
   * @brief computes the position of the EE in base frame, given the joint configuration
   * @param qr the JointCoordinates vector with the configuration of the joints
   * @param p_base the DataMap with the position of the EE in base frame
   */
	void GetFeetPoseBaseFromJoints(const JointCoordinates_t & qr , EEPositionsDataMap_t & p_base);

	/**
	 * @brief computes the EE velocity in base frame given the joint configuration and velocities
	 * @param qr the JointCoordinates vector with the configuration of the joints
	 * @param qrd the JointCoordinates vector with the joint velocities
	 * @param f_vel the DataMap with the velocitities of the EE in base frame
	 */
	void GetRealFeetVelocity(const JointCoordinates_t & qr , const JointCoordinates_t & qrd,
	                         EEPositionsDataMap_t & f_vel);

	/**
	 * @brief computes the position of the EE in base frame, given the body configuration
	 * @param big_q the GeneralizedCoordinate vector with the configuration of the robot
	 * @param p_base the DataMap with the position of the EE in base frame
	 */
  void GetFeetPoseBase(const GeneralizedCoordinates_t &big_q , EEPositionsDataMap_t & p_base);

  /**
   * @brief computes the position of the EE in base frame & in inertia frame, given the body configuration
   * @param big_q the GeneralizedCoordinate vector with the configuration of the robot
   * @param EEpose_inertia the DataMap with the position of the EE in inertia frame
   * @param EEpose_base the DataMap with the position of the EE in base frame
   */
  void GetFeetPose(const GeneralizedCoordinates_t & big_q , EEPositionsDataMap_t & EEpose_inertia , EEPositionsDataMap_t & EEpose_base);

  /**
   * @brief access method for the default distance of the base to the ground (assuming flat terrain).
   * @return the distance from the base to the ground.
   */
  double getDefaultGroundDistance();

};
#endif  /* PFLKINEMATICS_HPP_ */
