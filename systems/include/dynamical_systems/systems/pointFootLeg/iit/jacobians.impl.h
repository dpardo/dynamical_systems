
template <typename TRAIT>
iit::pfLeg::tpl::Jacobians<TRAIT>::Jacobians
    ()
     : 
    fr_trunk_J_LF_hipassemblyCOM(), 
    fr_trunk_J_LF_upperlegCOM(), 
    fr_trunk_J_LF_lowerlegCOM(), 
    fr_trunk_J_LF_foot()
{
    updateParameters();
}

template <typename TRAIT>
void iit::pfLeg::tpl::Jacobians<TRAIT>::updateParameters() {
}


template <typename TRAIT>
iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_hipassemblyCOM::Type_fr_trunk_J_LF_hipassemblyCOM()
{
    (*this)(0,0) = - 1.0;
    (*this)(1,0) = 0;
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(4,0) = 0;
    (*this)(5,0) = 0;
}

template <typename TRAIT>
const typename iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_hipassemblyCOM& iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_hipassemblyCOM::update(const JState& jState) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_upperlegCOM::Type_fr_trunk_J_LF_upperlegCOM()
{
    (*this)(0,0) = - 1.0;
    (*this)(0,1) = 0;
    (*this)(1,0) = 0;
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(4,1) = 0;
    (*this)(5,1) = 0;
}

template <typename TRAIT>
const typename iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_upperlegCOM& iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_upperlegCOM::update(const JState& jState) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( jState(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( jState(LF_HAA));
    
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(4,0) = (- 0.08 *  c_q_LF_HAA_);
    (*this)(5,0) = ( 0.08 *  s_q_LF_HAA_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_lowerlegCOM::Type_fr_trunk_J_LF_lowerlegCOM()
{
    (*this)(0,0) = - 1.0;
    (*this)(0,1) = 0;
    (*this)(0,2) = 0;
    (*this)(1,0) = 0;
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
}

template <typename TRAIT>
const typename iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_lowerlegCOM& iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_lowerlegCOM::update(const JState& jState) {
    Scalar s_q_LF_HAA_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_HAA_ = TRAIT::sin( jState(LF_HAA));
    s_q_LF_HFE_ = TRAIT::sin( jState(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( jState(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( jState(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( jState(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( jState(LF_KFE));
    
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,1) = (((( 0.125 *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.125 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - ( 0.35 *  c_q_LF_HFE_));
    (*this)(3,2) = ((( 0.125 *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.125 *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,0) = (((((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  c_q_LF_HAA_));
    (*this)(4,1) = ((((( 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,2) = (((( 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,0) = ((((((- 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.08 *  s_q_LF_HAA_));
    (*this)(5,1) = ((((( 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,2) = (((( 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_foot::Type_fr_trunk_J_LF_foot()
{
    (*this)(0,0) = - 1.0;
    (*this)(0,1) = 0;
    (*this)(0,2) = 0;
    (*this)(1,0) = 0;
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
}

template <typename TRAIT>
const typename iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_foot& iit::pfLeg::tpl::Jacobians<TRAIT>::Type_fr_trunk_J_LF_foot::update(const JState& jState) {
    Scalar s_q_LF_HAA_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_HAA_ = TRAIT::sin( jState(LF_HAA));
    s_q_LF_HFE_ = TRAIT::sin( jState(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( jState(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( jState(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( jState(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( jState(LF_KFE));
    
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,1) = (((( 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - ( 0.35 *  c_q_LF_HFE_));
    (*this)(3,2) = ((( 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,0) = (((((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  c_q_LF_HAA_));
    (*this)(4,1) = ((((( 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,2) = (((( 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,0) = ((((((- 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.08 *  s_q_LF_HAA_));
    (*this)(5,1) = ((((( 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) + (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,2) = (((( 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    return *this;
}
