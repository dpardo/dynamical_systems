#ifndef IIT_ROBOT_PFLEG_INERTIA_PROPERTIES_H_
#define IIT_ROBOT_PFLEG_INERTIA_PROPERTIES_H_

#include <iit/rbd/rbd.h>
#include <iit/rbd/InertiaMatrix.h>
#include <iit/rbd/utils.h>
#include <iit/rbd/traits/DoubleTrait.h>

#include "declarations.h"

namespace iit {
namespace pfLeg {
/**
 * This namespace encloses classes and functions related to the Dynamics
 * of the robot pfLeg.
 */
namespace dyn {

using InertiaMatrix = iit::rbd::InertiaMatrixDense;

namespace tpl {

template <typename TRAIT>
class InertiaProperties {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        typedef typename TRAIT::Scalar Scalar;
        typedef iit::rbd::Core<Scalar> CoreS;
        typedef iit::rbd::tpl::InertiaMatrixDense<Scalar> IMatrix;
        typedef typename CoreS::Vector3 Vec3d;

        InertiaProperties();
        ~InertiaProperties();
        const IMatrix& getTensor_trunk() const;
        const IMatrix& getTensor_LF_hipassembly() const;
        const IMatrix& getTensor_LF_upperleg() const;
        const IMatrix& getTensor_LF_lowerleg() const;
        Scalar getMass_trunk() const;
        Scalar getMass_LF_hipassembly() const;
        Scalar getMass_LF_upperleg() const;
        Scalar getMass_LF_lowerleg() const;
        const Vec3d& getCOM_trunk() const;
        const Vec3d& getCOM_LF_hipassembly() const;
        const Vec3d& getCOM_LF_upperleg() const;
        const Vec3d& getCOM_LF_lowerleg() const;
        Scalar getTotalMass() const;

    private:

        IMatrix tensor_trunk;
        IMatrix tensor_LF_hipassembly;
        IMatrix tensor_LF_upperleg;
        IMatrix tensor_LF_lowerleg;
        Vec3d com_trunk;
        Vec3d com_LF_hipassembly;
        Vec3d com_LF_upperleg;
        Vec3d com_LF_lowerleg;
};

template <typename TRAIT>
inline InertiaProperties<TRAIT>::~InertiaProperties() {}

template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::IMatrix& InertiaProperties<TRAIT>::getTensor_trunk() const {
    return this->tensor_trunk;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::IMatrix& InertiaProperties<TRAIT>::getTensor_LF_hipassembly() const {
    return this->tensor_LF_hipassembly;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::IMatrix& InertiaProperties<TRAIT>::getTensor_LF_upperleg() const {
    return this->tensor_LF_upperleg;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::IMatrix& InertiaProperties<TRAIT>::getTensor_LF_lowerleg() const {
    return this->tensor_LF_lowerleg;
}
template <typename TRAIT>
inline typename InertiaProperties<TRAIT>::Scalar InertiaProperties<TRAIT>::getMass_trunk() const {
    return this->tensor_trunk.getMass();
}
template <typename TRAIT>
inline typename InertiaProperties<TRAIT>::Scalar InertiaProperties<TRAIT>::getMass_LF_hipassembly() const {
    return this->tensor_LF_hipassembly.getMass();
}
template <typename TRAIT>
inline typename InertiaProperties<TRAIT>::Scalar InertiaProperties<TRAIT>::getMass_LF_upperleg() const {
    return this->tensor_LF_upperleg.getMass();
}
template <typename TRAIT>
inline typename InertiaProperties<TRAIT>::Scalar InertiaProperties<TRAIT>::getMass_LF_lowerleg() const {
    return this->tensor_LF_lowerleg.getMass();
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::Vec3d& InertiaProperties<TRAIT>::getCOM_trunk() const {
    return this->com_trunk;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::Vec3d& InertiaProperties<TRAIT>::getCOM_LF_hipassembly() const {
    return this->com_LF_hipassembly;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::Vec3d& InertiaProperties<TRAIT>::getCOM_LF_upperleg() const {
    return this->com_LF_upperleg;
}
template <typename TRAIT>
inline const typename InertiaProperties<TRAIT>::Vec3d& InertiaProperties<TRAIT>::getCOM_LF_lowerleg() const {
    return this->com_LF_lowerleg;
}

template <typename TRAIT>
inline typename InertiaProperties<TRAIT>::Scalar InertiaProperties<TRAIT>::getTotalMass() const {
    return 10.0 + 3.44 + 3.146 + 0.881;
}

}

using InertiaProperties = tpl::InertiaProperties<rbd::DoubleTrait>;

}
}
}

#include "inertia_properties.impl.h"

#endif
