
// Constructors
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::MotionTransforms
    ()
     :
    fr_trunk_X_fr_LF_hipassembly(),
    fr_trunk_X_fr_LF_upperleg(),
    fr_trunk_X_fr_LF_lowerleg(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_LF_upperleg_X_fr_trunk(),
    fr_LF_lowerleg_X_fr_trunk(),
    fr_trunk_X_LF_hipassemblyCOM(),
    fr_trunk_X_LF_upperlegCOM(),
    fr_trunk_X_LF_lowerlegCOM(),
    LF_foot_X_fr_LF_lowerleg(),
    fr_trunk_X_LF_foot(),
    LF_foot_X_fr_trunk(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg()
{
    updateParameters();
}
template <typename TRAIT>
void iit::pfLeg::tpl::MotionTransforms<TRAIT>::updateParameters() {
}

template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::ForceTransforms
    ()
     :
    fr_trunk_X_fr_LF_hipassembly(),
    fr_trunk_X_fr_LF_upperleg(),
    fr_trunk_X_fr_LF_lowerleg(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_LF_upperleg_X_fr_trunk(),
    fr_LF_lowerleg_X_fr_trunk(),
    fr_trunk_X_LF_hipassemblyCOM(),
    fr_trunk_X_LF_upperlegCOM(),
    fr_trunk_X_LF_lowerlegCOM(),
    LF_foot_X_fr_LF_lowerleg(),
    fr_trunk_X_LF_foot(),
    LF_foot_X_fr_trunk(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg()
{
    updateParameters();
}
template <typename TRAIT>
void iit::pfLeg::tpl::ForceTransforms<TRAIT>::updateParameters() {
}

template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::HomogeneousTransforms
    ()
     :
    fr_trunk_X_fr_LF_hipassembly(),
    fr_trunk_X_fr_LF_upperleg(),
    fr_trunk_X_fr_LF_lowerleg(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_LF_upperleg_X_fr_trunk(),
    fr_LF_lowerleg_X_fr_trunk(),
    fr_trunk_X_LF_hipassemblyCOM(),
    fr_trunk_X_LF_upperlegCOM(),
    fr_trunk_X_LF_lowerlegCOM(),
    LF_foot_X_fr_LF_lowerleg(),
    fr_trunk_X_LF_foot(),
    LF_foot_X_fr_trunk(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg()
{
    updateParameters();
}
template <typename TRAIT>
void iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::updateParameters() {
}

template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,2) = 0.15;
    (*this)(4,5) = 0;
    (*this)(5,2) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    (*this)(3,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(3,1) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,0) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(4,1) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(5,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(5,1) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,4) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::Type_fr_trunk_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(3,1) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(3,2) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,0) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(4,1) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,2) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(5,1) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(5,2) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::Type_fr_trunk_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,1) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,2) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,0) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(4,1) = ((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(4,2) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,3) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(5,1) = ((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(5,2) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,3) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,3) = 0;
    (*this)(4,3) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0.15;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1.0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,1) = - s_q_LF_HAA_;
    (*this)(0,2) = - c_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(1,2) =  s_q_LF_HAA_;
    (*this)(3,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(3,1) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(3,2) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(3,4) = - s_q_LF_HAA_;
    (*this)(3,5) = - c_q_LF_HAA_;
    (*this)(4,0) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,1) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(4,2) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(4,5) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::Type_fr_LF_upperleg_X_fr_trunk()
{
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(5,3) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(0,2) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,0) = - c_q_LF_HFE_;
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(3,1) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(3,2) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(3,5) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,0) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(4,1) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,2) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(4,3) = - c_q_LF_HFE_;
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,0) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(5,1) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(5,2) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(5,4) =  c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::Type_fr_LF_lowerleg_X_fr_trunk()
{
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(5,3) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,0) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,1) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(3,2) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,5) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,0) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,1) = ((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(4,2) = ((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(4,3) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,0) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(5,1) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,2) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,4) =  c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::Type_fr_trunk_X_LF_hipassemblyCOM()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,2) = 0.15;
    (*this)(4,5) = 0;
    (*this)(5,2) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    (*this)(3,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(3,1) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,0) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(4,1) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(5,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(5,1) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,4) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::Type_fr_trunk_X_LF_upperlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(3,1) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(3,2) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,0) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(4,1) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,2) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(5,1) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(5,2) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::Type_fr_trunk_X_LF_lowerlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,1) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,2) = ((((((- 0.125 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.125 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,0) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(4,1) = (((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.125 *  c_q_LF_HAA_));
    (*this)(4,2) = ((((((- 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(4,3) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(5,1) = (((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.125 *  s_q_LF_HAA_));
    (*this)(5,2) = ((((((- 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(5,3) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::Type_LF_foot_X_fr_LF_lowerleg()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1.0;
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = - 1;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = - 0.33;
    (*this)(3,3) = 0;
    (*this)(3,4) = - 1;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = - 0.33;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(4,4) = 0;
    (*this)(4,5) = 1;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,4) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_foot& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,2) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,0) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,0) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(3,0) = ((((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,1) = ((((((- 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(3,2) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(3,3) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(3,5) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,0) = ((((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.33 *  c_q_LF_HAA_));
    (*this)(4,1) = ((((((- 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(4,2) = (((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + (((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(4,3) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) =  c_q_LF_HAA_;
    (*this)(4,5) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(5,0) = ((((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.33 *  s_q_LF_HAA_));
    (*this)(5,1) = ((((((- 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(5,2) = ((((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + (((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(5,3) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = - s_q_LF_HAA_;
    (*this)(5,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::Type_LF_foot_X_fr_trunk()
{
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(4,3) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_trunk& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,1) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = - s_q_LF_HAA_;
    (*this)(2,0) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(2,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(3,0) = ((((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,1) = ((((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.33 *  c_q_LF_HAA_));
    (*this)(3,2) = ((((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.33 *  s_q_LF_HAA_));
    (*this)(3,3) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(3,4) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,5) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,0) = ((((((- 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(4,1) = ((((((- 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(4,2) = ((((((- 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(4,4) =  c_q_LF_HAA_;
    (*this)(4,5) = - s_q_LF_HAA_;
    (*this)(5,0) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(5,1) = (((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + (((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(5,2) = ((((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + (((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(5,3) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(5,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(5,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = - 1.0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = - 0.15;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,0) = 0.15;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0.15;
    (*this)(4,3) = 0;
    (*this)(4,4) = - 1.0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = - 0.15;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1.0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1.0;
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,1) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,1) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,1) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = - 1.0;
    (*this)(3,5) = 0;
    (*this)(4,4) = 0;
    (*this)(5,4) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(3,2) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(4,0) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(4,1) = (( 0.08 *  c_q_LF_HAA_) +  0.15);
    (*this)(4,2) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(5,1) = (- 0.08 *  s_q_LF_HAA_);
    (*this)(5,2) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(3,0) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(3,1) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(3,2) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,0) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(4,1) = ((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_));
    (*this)(4,2) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,0) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(5,1) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_));
    (*this)(5,2) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,1) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = - 1.0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,2) = 0;
    (*this)(3,4) = 0;
    (*this)(4,0) = 0;
    (*this)(4,2) = 0;
    (*this)(4,4) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = - 0.08;
    (*this)(5,3) = 0;
    (*this)(5,4) = - 1;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,2) =  s_q_LF_HFE_;
    (*this)(1,0) = - s_q_LF_HFE_;
    (*this)(1,2) =  c_q_LF_HFE_;
    (*this)(3,1) = (- 0.08 *  s_q_LF_HFE_);
    (*this)(3,3) =  c_q_LF_HFE_;
    (*this)(3,5) =  s_q_LF_HFE_;
    (*this)(4,1) = (- 0.08 *  c_q_LF_HFE_);
    (*this)(4,3) = - s_q_LF_HFE_;
    (*this)(4,5) =  c_q_LF_HFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = - 1;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(4,4) = 0;
    (*this)(4,5) = - 1.0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = - 0.08;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,1) = - s_q_LF_HFE_;
    (*this)(2,0) =  s_q_LF_HFE_;
    (*this)(2,1) =  c_q_LF_HFE_;
    (*this)(3,3) =  c_q_LF_HFE_;
    (*this)(3,4) = - s_q_LF_HFE_;
    (*this)(4,0) = (- 0.08 *  s_q_LF_HFE_);
    (*this)(4,1) = (- 0.08 *  c_q_LF_HFE_);
    (*this)(5,3) =  s_q_LF_HFE_;
    (*this)(5,4) =  c_q_LF_HFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = - 0.35;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 1;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) =  s_q_LF_KFE_;
    (*this)(1,0) = - s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    (*this)(3,2) = ( 0.35 *  s_q_LF_KFE_);
    (*this)(3,3) =  c_q_LF_KFE_;
    (*this)(3,4) =  s_q_LF_KFE_;
    (*this)(4,2) = ( 0.35 *  c_q_LF_KFE_);
    (*this)(4,3) = - s_q_LF_KFE_;
    (*this)(4,4) =  c_q_LF_KFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = - 0.35;
    (*this)(4,5) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg& iit::pfLeg::tpl::MotionTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) = - s_q_LF_KFE_;
    (*this)(1,0) =  s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    (*this)(3,3) =  c_q_LF_KFE_;
    (*this)(3,4) = - s_q_LF_KFE_;
    (*this)(4,3) =  s_q_LF_KFE_;
    (*this)(4,4) =  c_q_LF_KFE_;
    (*this)(5,0) = ( 0.35 *  s_q_LF_KFE_);
    (*this)(5,1) = ( 0.35 *  c_q_LF_KFE_);
    return *this;
}

template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,5) = 0.15;
    (*this)(2,2) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(0,4) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(1,3) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(1,4) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    (*this)(2,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,4) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,4) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::Type_fr_trunk_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(0,3) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(0,4) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(0,5) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(1,4) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(1,5) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,4) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,5) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::Type_fr_trunk_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,4) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,5) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(1,4) = ((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(1,5) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(2,4) = ((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(2,5) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,3) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0;
    (*this)(1,0) = 0;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0.15;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1.0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,1) = - s_q_LF_HAA_;
    (*this)(0,2) = - c_q_LF_HAA_;
    (*this)(0,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(0,4) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(0,5) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(1,2) =  s_q_LF_HAA_;
    (*this)(1,3) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(1,4) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(1,5) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(3,4) = - s_q_LF_HAA_;
    (*this)(3,5) = - c_q_LF_HAA_;
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(4,5) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::Type_fr_LF_upperleg_X_fr_trunk()
{
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(0,2) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(0,3) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(0,4) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(0,5) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(1,0) = - c_q_LF_HFE_;
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,3) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(1,4) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(1,5) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(2,4) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(2,5) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(3,5) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,3) = - c_q_LF_HFE_;
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,4) =  c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::Type_fr_LF_lowerleg_X_fr_trunk()
{
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,3) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,4) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(0,5) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(1,0) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,3) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,4) = ((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(1,5) = ((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_));
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(2,4) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(2,5) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,5) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,3) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) =  c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::Type_fr_trunk_X_LF_hipassemblyCOM()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,5) = 0.15;
    (*this)(2,2) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(0,4) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(1,3) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(1,4) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    (*this)(2,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,4) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,4) = - c_q_LF_HAA_;
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,4) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::Type_fr_trunk_X_LF_upperlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(0,3) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(0,4) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(0,5) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(1,4) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(1,5) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,4) = ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,5) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::Type_fr_trunk_X_LF_lowerlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,4) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) + ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,5) = ((((((- 0.125 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.125 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = ((((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(1,4) = (((((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.125 *  c_q_LF_HAA_));
    (*this)(1,5) = ((((((- 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + ((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(2,4) = (((((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.125 *  s_q_LF_HAA_));
    (*this)(2,5) = ((((((- 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(3,3) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(3,4) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,3) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::Type_LF_foot_X_fr_LF_lowerleg()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1;
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = - 0.33;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = 1;
    (*this)(1,3) = 0;
    (*this)(1,4) = - 0.33;
    (*this)(1,5) = 0;
    (*this)(2,0) = - 1;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = - 1.0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(4,4) = 0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,4) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_foot& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,2) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = ((((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,4) = ((((((- 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(0,5) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(1,0) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(1,3) = ((((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.33 *  c_q_LF_HAA_));
    (*this)(1,4) = ((((((- 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(1,5) = (((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + (((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(2,0) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,3) = ((((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.33 *  s_q_LF_HAA_));
    (*this)(2,4) = ((((((- 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(2,5) = ((((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + (((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(3,3) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(3,5) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(4,3) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) =  c_q_LF_HAA_;
    (*this)(4,5) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(5,3) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(5,4) = - s_q_LF_HAA_;
    (*this)(5,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::Type_LF_foot_X_fr_trunk()
{
    (*this)(1,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_trunk& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,1) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,3) = ((((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,4) = ((((((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  c_q_LF_KFE_)) - ( 0.33 *  c_q_LF_HAA_));
    (*this)(0,5) = ((((((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  c_q_LF_KFE_)) + ( 0.33 *  s_q_LF_HAA_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = - s_q_LF_HAA_;
    (*this)(1,3) = ((((((- 0.33 *  s_q_LF_HFE_) *  s_q_LF_KFE_) + (( 0.33 *  c_q_LF_HFE_) *  c_q_LF_KFE_)) + ( 0.35 *  c_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(1,4) = ((((((- 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  s_q_LF_HAA_));
    (*this)(1,5) = ((((((- 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_)) + ( 0.15 *  c_q_LF_HAA_));
    (*this)(2,0) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(2,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,3) = (((( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_) - ((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,4) = (((((( 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + (((- 0.08 *  c_q_LF_HAA_) -  0.15) *  c_q_LF_HFE_)) - ( 0.35 *  c_q_LF_HAA_)) *  s_q_LF_KFE_) + (((((- 0.08 *  c_q_LF_HAA_) -  0.15) *  s_q_LF_HFE_) - (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(2,5) = ((((((- 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) + ( 0.35 *  s_q_LF_HAA_)) *  s_q_LF_KFE_) + (((( 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) + (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(3,3) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(3,4) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(3,5) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(4,4) =  c_q_LF_HAA_;
    (*this)(4,5) = - s_q_LF_HAA_;
    (*this)(5,3) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(5,4) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(5,5) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0;
    (*this)(0,4) = - 0.15;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = - 1.0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0.15;
    (*this)(1,4) = 0;
    (*this)(1,5) = 0.15;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = - 0.15;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = 0;
    (*this)(3,5) = - 1.0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(4,4) = - 1.0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = - 1.0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1.0;
    (*this)(0,2) = 0;
    (*this)(0,4) = 0;
    (*this)(1,1) = 0;
    (*this)(2,1) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 0;
    (*this)(3,4) = - 1.0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,4) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,4) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(0,5) = (( 0.15 *  c_q_LF_HAA_) +  0.08);
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(1,4) = (( 0.08 *  c_q_LF_HAA_) +  0.15);
    (*this)(1,5) = ( 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,4) = (- 0.08 *  s_q_LF_HAA_);
    (*this)(2,5) = ( 0.15 *  c_q_LF_HAA_);
    (*this)(4,3) = - s_q_LF_HAA_;
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = - c_q_LF_HAA_;
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(0,3) = ((- 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_);
    (*this)(0,4) = (( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_);
    (*this)(0,5) = ((( 0.35 *  c_q_LF_HFE_) + ( 0.15 *  c_q_LF_HAA_)) +  0.08);
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((( 0.08 *  c_q_LF_HAA_) +  0.15) *  s_q_LF_HFE_) + (( 0.15 *  c_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(1,4) = ((((- 0.15 *  c_q_LF_HAA_) *  s_q_LF_HFE_) + ((( 0.08 *  c_q_LF_HAA_) +  0.15) *  c_q_LF_HFE_)) + ( 0.35 *  c_q_LF_HAA_));
    (*this)(1,5) = (( 0.15 *  s_q_LF_HAA_) - (( 0.35 *  s_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (((- 0.08 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.15 *  s_q_LF_HAA_) *  c_q_LF_HFE_));
    (*this)(2,4) = (((( 0.15 *  s_q_LF_HAA_) *  s_q_LF_HFE_) - (( 0.08 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.35 *  s_q_LF_HAA_));
    (*this)(2,5) = (( 0.15 *  c_q_LF_HAA_) - (( 0.35 *  c_q_LF_HAA_) *  s_q_LF_HFE_));
    (*this)(3,3) = - s_q_LF_HFE_;
    (*this)(3,4) = - c_q_LF_HFE_;
    (*this)(4,3) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(4,4) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(4,5) =  c_q_LF_HAA_;
    (*this)(5,3) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(5,4) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(5,5) = - s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0;
    (*this)(0,3) = 0;
    (*this)(0,5) = 0;
    (*this)(1,1) = 0;
    (*this)(1,3) = 0;
    (*this)(1,5) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = - 1;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = - 0.08;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,4) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,4) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
    (*this)(5,4) = - 1.0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,2) =  s_q_LF_HFE_;
    (*this)(0,4) = (- 0.08 *  s_q_LF_HFE_);
    (*this)(1,0) = - s_q_LF_HFE_;
    (*this)(1,2) =  c_q_LF_HFE_;
    (*this)(1,4) = (- 0.08 *  c_q_LF_HFE_);
    (*this)(3,3) =  c_q_LF_HFE_;
    (*this)(3,5) =  s_q_LF_HFE_;
    (*this)(4,3) = - s_q_LF_HFE_;
    (*this)(4,5) =  c_q_LF_HFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = - 1.0;
    (*this)(1,5) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(2,4) = 0;
    (*this)(2,5) = - 0.08;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,3) = 0;
    (*this)(4,4) = 0;
    (*this)(4,5) = - 1;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,5) = 0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,1) = - s_q_LF_HFE_;
    (*this)(1,3) = (- 0.08 *  s_q_LF_HFE_);
    (*this)(1,4) = (- 0.08 *  c_q_LF_HFE_);
    (*this)(2,0) =  s_q_LF_HFE_;
    (*this)(2,1) =  c_q_LF_HFE_;
    (*this)(3,3) =  c_q_LF_HFE_;
    (*this)(3,4) = - s_q_LF_HFE_;
    (*this)(5,3) =  s_q_LF_HFE_;
    (*this)(5,4) =  c_q_LF_HFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1;
    (*this)(2,3) = 0;
    (*this)(2,4) = - 0.35;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) =  s_q_LF_KFE_;
    (*this)(0,5) = ( 0.35 *  s_q_LF_KFE_);
    (*this)(1,0) = - s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    (*this)(1,5) = ( 0.35 *  c_q_LF_KFE_);
    (*this)(3,3) =  c_q_LF_KFE_;
    (*this)(3,4) =  s_q_LF_KFE_;
    (*this)(4,3) = - s_q_LF_KFE_;
    (*this)(4,4) =  c_q_LF_KFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(0,4) = 0;
    (*this)(0,5) = 0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(1,4) = 0;
    (*this)(1,5) = - 0.35;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1.0;
    (*this)(2,5) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,5) = 0;
    (*this)(4,0) = 0;
    (*this)(4,1) = 0;
    (*this)(4,2) = 0;
    (*this)(4,5) = 0;
    (*this)(5,0) = 0;
    (*this)(5,1) = 0;
    (*this)(5,2) = 0;
    (*this)(5,3) = 0;
    (*this)(5,4) = 0;
    (*this)(5,5) = 1;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg& iit::pfLeg::tpl::ForceTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) = - s_q_LF_KFE_;
    (*this)(1,0) =  s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    (*this)(2,3) = ( 0.35 *  s_q_LF_KFE_);
    (*this)(2,4) = ( 0.35 *  c_q_LF_KFE_);
    (*this)(3,3) =  c_q_LF_KFE_;
    (*this)(3,4) = - s_q_LF_KFE_;
    (*this)(4,3) =  s_q_LF_KFE_;
    (*this)(4,4) =  c_q_LF_KFE_;
    return *this;
}

template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0.15;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = - 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::Type_fr_trunk_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (- 0.08 *  s_q_LF_HAA_);
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((- 0.08 *  c_q_LF_HAA_) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::Type_fr_trunk_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = ( 0.15 - ( 0.35 *  s_q_LF_HFE_));
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((- 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_) - ( 0.08 *  s_q_LF_HAA_));
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((((- 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_) - ( 0.08 *  c_q_LF_HAA_)) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0;
    (*this)(1,0) = 0;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,1) = - s_q_LF_HAA_;
    (*this)(0,2) = - c_q_LF_HAA_;
    (*this)(0,3) = (- 0.15 *  c_q_LF_HAA_);
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(1,2) =  s_q_LF_HAA_;
    (*this)(1,3) = ( 0.15 *  s_q_LF_HAA_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::Type_fr_LF_upperleg_X_fr_trunk()
{
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(0,2) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(0,3) = (( 0.15 *  s_q_LF_HFE_) + (((- 0.15 *  c_q_LF_HAA_) -  0.08) *  c_q_LF_HFE_));
    (*this)(1,0) = - c_q_LF_HFE_;
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,3) = (((( 0.15 *  c_q_LF_HAA_) +  0.08) *  s_q_LF_HFE_) + ( 0.15 *  c_q_LF_HFE_));
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (- 0.15 *  s_q_LF_HAA_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::Type_fr_LF_lowerleg_X_fr_trunk()
{
    (*this)(2,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,3) = (((((( 0.15 *  c_q_LF_HAA_) +  0.08) *  s_q_LF_HFE_) + ( 0.15 *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + (((( 0.15 *  s_q_LF_HFE_) + (((- 0.15 *  c_q_LF_HAA_) -  0.08) *  c_q_LF_HFE_)) -  0.35) *  c_q_LF_KFE_));
    (*this)(1,0) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,3) = (((((- 0.15 *  s_q_LF_HFE_) + ((( 0.15 *  c_q_LF_HAA_) +  0.08) *  c_q_LF_HFE_)) +  0.35) *  s_q_LF_KFE_) + ((((( 0.15 *  c_q_LF_HAA_) +  0.08) *  s_q_LF_HFE_) + ( 0.15 *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(2,1) =  c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = (- 0.15 *  s_q_LF_HAA_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::Type_fr_trunk_X_LF_hipassemblyCOM()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0.15;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = - 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_hipassemblyCOM::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,1) = - c_q_LF_HAA_;
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,1) =  s_q_LF_HAA_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::Type_fr_trunk_X_LF_upperlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_upperlegCOM::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (- 0.08 *  s_q_LF_HAA_);
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((- 0.08 *  c_q_LF_HAA_) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::Type_fr_trunk_X_LF_lowerlegCOM()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_lowerlegCOM::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = ((- c_q_LF_HFE_ *  s_q_LF_KFE_) - ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,1) = (( s_q_LF_HFE_ *  s_q_LF_KFE_) - ( c_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = (((((- 0.125 *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.125 *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - ( 0.35 *  s_q_LF_HFE_)) +  0.15);
    (*this)(1,0) = ((( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((((( 0.125 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  s_q_LF_HAA_));
    (*this)(2,0) = ((( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) + (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((((((( 0.125 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.125 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  c_q_LF_HAA_)) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::Type_LF_foot_X_fr_LF_lowerleg()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1;
    (*this)(0,2) = 0;
    (*this)(0,3) = 0;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = 1;
    (*this)(1,3) = 0;
    (*this)(2,0) = - 1;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0.33;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_LF_lowerleg::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_foot& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_LF_foot::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,2) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(0,3) = (((((- 0.33 *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( 0.33 *  s_q_LF_HFE_) *  c_q_LF_KFE_)) - ( 0.35 *  s_q_LF_HFE_)) +  0.15);
    (*this)(1,0) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(1,3) = (((((( 0.33 *  s_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  s_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  s_q_LF_HAA_));
    (*this)(2,0) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(2,1) = - s_q_LF_HAA_;
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,3) = ((((((( 0.33 *  c_q_LF_HAA_) *  s_q_LF_HFE_) *  s_q_LF_KFE_) - ((( 0.33 *  c_q_LF_HAA_) *  c_q_LF_HFE_) *  c_q_LF_KFE_)) - (( 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_)) - ( 0.08 *  c_q_LF_HAA_)) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::Type_LF_foot_X_fr_trunk()
{
    (*this)(1,0) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_trunk& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_LF_foot_X_fr_trunk::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_KFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_KFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = (( c_q_LF_HFE_ *  c_q_LF_KFE_) - ( s_q_LF_HFE_ *  s_q_LF_KFE_));
    (*this)(0,1) = (((- s_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,2) = (((- c_q_LF_HAA_ *  c_q_LF_HFE_) *  s_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  c_q_LF_KFE_));
    (*this)(0,3) = ((((( 0.15 *  s_q_LF_HFE_) + (((- 0.15 *  c_q_LF_HAA_) -  0.08) *  c_q_LF_HFE_)) -  0.35) *  s_q_LF_KFE_) + (((((- 0.15 *  c_q_LF_HAA_) -  0.08) *  s_q_LF_HFE_) - ( 0.15 *  c_q_LF_HFE_)) *  c_q_LF_KFE_));
    (*this)(1,1) =  c_q_LF_HAA_;
    (*this)(1,2) = - s_q_LF_HAA_;
    (*this)(1,3) = (- 0.15 *  s_q_LF_HAA_);
    (*this)(2,0) = (( c_q_LF_HFE_ *  s_q_LF_KFE_) + ( s_q_LF_HFE_ *  c_q_LF_KFE_));
    (*this)(2,1) = ((( s_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( s_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,2) = ((( c_q_LF_HAA_ *  c_q_LF_HFE_) *  c_q_LF_KFE_) - (( c_q_LF_HAA_ *  s_q_LF_HFE_) *  s_q_LF_KFE_));
    (*this)(2,3) = (((((((- 0.15 *  c_q_LF_HAA_) -  0.08) *  s_q_LF_HFE_) - ( 0.15 *  c_q_LF_HFE_)) *  s_q_LF_KFE_) + ((((- 0.15 *  s_q_LF_HFE_) + ((( 0.15 *  c_q_LF_HAA_) +  0.08) *  c_q_LF_HFE_)) +  0.35) *  c_q_LF_KFE_)) +  0.33);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = 0;
    (*this)(0,2) = - 1.0;
    (*this)(0,3) = 0.15;
    (*this)(1,0) = 0;
    (*this)(1,1) = - 1.0;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(2,0) = - 1.0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = - 0.15;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HAA::update(const JState& q) {
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0;
    (*this)(0,1) = - 1.0;
    (*this)(0,2) = 0;
    (*this)(0,3) = 0.15;
    (*this)(1,1) = 0;
    (*this)(2,1) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_HFE::update(const JState& q) {
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(1,0) = - s_q_LF_HAA_;
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (- 0.08 *  s_q_LF_HAA_);
    (*this)(2,0) = - c_q_LF_HAA_;
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((- 0.08 *  c_q_LF_HAA_) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_trunk_X_fr_LF_KFE::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar s_q_LF_HAA_;
    Scalar c_q_LF_HFE_;
    Scalar c_q_LF_HAA_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    s_q_LF_HAA_ = TRAIT::sin( q(LF_HAA));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    c_q_LF_HAA_ = TRAIT::cos( q(LF_HAA));
    
    (*this)(0,0) = - s_q_LF_HFE_;
    (*this)(0,1) = - c_q_LF_HFE_;
    (*this)(0,3) = ( 0.15 - ( 0.35 *  s_q_LF_HFE_));
    (*this)(1,0) = (- s_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(1,1) = ( s_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(1,2) =  c_q_LF_HAA_;
    (*this)(1,3) = (((- 0.35 *  s_q_LF_HAA_) *  c_q_LF_HFE_) - ( 0.08 *  s_q_LF_HAA_));
    (*this)(2,0) = (- c_q_LF_HAA_ *  c_q_LF_HFE_);
    (*this)(2,1) = ( c_q_LF_HAA_ *  s_q_LF_HFE_);
    (*this)(2,2) = - s_q_LF_HAA_;
    (*this)(2,3) = ((((- 0.35 *  c_q_LF_HAA_) *  c_q_LF_HFE_) - ( 0.08 *  c_q_LF_HAA_)) -  0.15);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0;
    (*this)(1,1) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = - 1;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,2) =  s_q_LF_HFE_;
    (*this)(0,3) = (- 0.08 *  c_q_LF_HFE_);
    (*this)(1,0) = - s_q_LF_HFE_;
    (*this)(1,2) =  c_q_LF_HFE_;
    (*this)(1,3) = ( 0.08 *  s_q_LF_HFE_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0.08;
    (*this)(1,0) = 0;
    (*this)(1,1) = 0;
    (*this)(1,2) = - 1;
    (*this)(1,3) = 0;
    (*this)(2,2) = 0;
    (*this)(2,3) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_HFE_;
    Scalar c_q_LF_HFE_;
    
    s_q_LF_HFE_ = TRAIT::sin( q(LF_HFE));
    c_q_LF_HFE_ = TRAIT::cos( q(LF_HFE));
    
    (*this)(0,0) =  c_q_LF_HFE_;
    (*this)(0,1) = - s_q_LF_HFE_;
    (*this)(2,0) =  s_q_LF_HFE_;
    (*this)(2,1) =  c_q_LF_HFE_;
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0;
    (*this)(1,2) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1;
    (*this)(2,3) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1.0;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) =  s_q_LF_KFE_;
    (*this)(0,3) = (- 0.35 *  c_q_LF_KFE_);
    (*this)(1,0) = - s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    (*this)(1,3) = ( 0.35 *  s_q_LF_KFE_);
    return *this;
}
template <typename TRAIT>
iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0;
    (*this)(0,3) = 0.35;
    (*this)(1,2) = 0;
    (*this)(1,3) = 0;
    (*this)(2,0) = 0;
    (*this)(2,1) = 0;
    (*this)(2,2) = 1;
    (*this)(2,3) = 0;
    (*this)(3,0) = 0;
    (*this)(3,1) = 0;
    (*this)(3,2) = 0;
    (*this)(3,3) = 1;
}
template <typename TRAIT>
const typename iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg& iit::pfLeg::tpl::HomogeneousTransforms<TRAIT>::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const JState& q) {
    Scalar s_q_LF_KFE_;
    Scalar c_q_LF_KFE_;
    
    s_q_LF_KFE_ = TRAIT::sin( q(LF_KFE));
    c_q_LF_KFE_ = TRAIT::cos( q(LF_KFE));
    
    (*this)(0,0) =  c_q_LF_KFE_;
    (*this)(0,1) = - s_q_LF_KFE_;
    (*this)(1,0) =  s_q_LF_KFE_;
    (*this)(1,1) =  c_q_LF_KFE_;
    return *this;
}

