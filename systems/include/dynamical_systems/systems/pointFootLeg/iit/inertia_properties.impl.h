template <typename TRAIT>
iit::pfLeg::dyn::tpl::InertiaProperties<TRAIT>::InertiaProperties()
{
    com_trunk = iit::rbd::Vector3d(0.0,0.0,0.0).cast<Scalar>();
    tensor_trunk.fill(
        Scalar(10.0),
        com_trunk,
        rbd::Utils::buildInertiaTensor(
                Scalar(0.083),
                Scalar(0.083),
                Scalar(0.15),
                Scalar(0.0),
                Scalar(0.0),
                Scalar(0.0)) );

    com_LF_hipassembly = iit::rbd::Vector3d(0.0,0.0,0.0).cast<Scalar>();
    tensor_LF_hipassembly.fill(
        Scalar(3.44),
        com_LF_hipassembly,
        rbd::Utils::buildInertiaTensor(
                Scalar(0.134705),
                Scalar(0.144171),
                Scalar(0.011033),
                Scalar(0.0),
                Scalar(0.0),
                Scalar(0.0)) );

    com_LF_upperleg = iit::rbd::Vector3d(0.15074,-0.02625,-0.0).cast<Scalar>();
    tensor_LF_upperleg.fill(
        Scalar(3.146),
        com_LF_upperleg,
        rbd::Utils::buildInertiaTensor(
                Scalar(0.005495),
                Scalar(0.087136),
                Scalar(0.089871),
                Scalar(0.0),
                Scalar(0.0),
                Scalar(0.0)) );

    com_LF_lowerleg = iit::rbd::Vector3d(0.1254,5.0E-4,-1.0E-4).cast<Scalar>();
    tensor_LF_lowerleg.fill(
        Scalar(0.881),
        com_LF_lowerleg,
        rbd::Utils::buildInertiaTensor(
                Scalar(4.68E-4),
                Scalar(0.026409),
                Scalar(0.026181),
                Scalar(0.0),
                Scalar(0.0),
                Scalar(0.0)) );

}

