/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * PFLExtendedJacobians.hpp
 *
 *  Created on: August 4, 2016
 *      Author: depardo
 */

#ifndef PFLExtendedJacobian_HPP_
#define PFLExtendedJacobian_HPP_

#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>

/**
 * @brief: This class represents the EE Jacobians of a robot independent of
 * the contact configuration.
 */
class PFLExtendedJacobians : public PFLKinematics {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef robotDimensions::EEPositionsDataMap_t EEPositionsDataMap_t;
	typedef robotDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef robotDimensions::JointCoordinates_t JointCoordinates_t;
	typedef robotDimensions::SingleEEBaseJacobian_t SingleEEBaseJacobian_t;
	typedef robotDimensions::SingleEEJointJacobian_t SingleEEJointJacobian_t;

	/**
	 * @brief The constructor
	 */
	PFLExtendedJacobians();
	virtual ~PFLExtendedJacobians();

	/**
	 * @brief Update the Jacobians (Jb,Jr) given the configuration of the robot
	 * @param big_q The generalized coordinates vector with the configuration of the robot
	 */
	void updateJacobians(const GeneralizedCoordinates_t & big_q);

	/**
	 * @brief Update the Jacobians (Jb,Jr) given the joint configuration
	 * @param joints_q_r The vector of joint coordinates
	 */
	void updateJacobians(const JointCoordinates_t & joints_q_r);

	/**
	 * @brief Updates the Jacobians given the configuration of the joints and the position
	 * of the EE (base reference frame). CAUTION: This method doesn't check that joints
	 * configuration and ee_pose correspond to each other.
	 * @param joints_q_r The joint coordinates vector
	 * @param ee_pose_base The position of the EE expressed in Base frame
	 */
	void updateJacobiansQuick(const JointCoordinates_t & joints_q_r,
		const EEPositionsDataMap_t & ee_pose_base);

	/**
	 * @brief Updates Jr given the configuration of the robot
	 * @param big_q The generalized coordinates vector with the configuration of the robot
	 */
	void updateActuatedJacobians(const GeneralizedCoordinates_t & big_q);

	/**
	 * @brief Updates Jr given the configuration of the joints
	 * @param joints_q_r The vector of joint coordinates
	 */
	void updateAJ_FromJoints(const JointCoordinates_t & joints_q_r);

	/**
	 * @brief Updates Jb given the configuration of the robot
	 * @param big_q The vector of generalized coordinates with the configuration of the robot
	 */
	void updateFloatingBaseJacobians(const GeneralizedCoordinates_t & big_q);

	/**
	 * @brief Updates Jb given the position of the EE
	 * @param eet_pose_base The position of the EE expressed in Base Frame
	 */
	void updateFBJ_FromFeetPose(const EEPositionsDataMap_t & eet_pose_base);


  SingleEEBaseJacobian_t Jb_LF;   /*!< First block <3x6> of the EE_XX Jacobian, relating base velocity with EE velocity  */
  SingleEEJointJacobian_t Jr_LF;  /*!< Second block <3xn_joints> of the EE_XX Jacobian, relating joint velocities with EE velocity */
  Eigen::Matrix3d skew_matrix_foot_postion; /*!< a skew_matrix container */

};

#endif /* PFLExtendedJacobianS_HPP_ */
