/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/


/*
 * acrobot_dynamics.hpp
 *
 *  Created on: Jun 6, 2015
 *      Author: depardo
 */

#ifndef ACROBOTDYNAMICS_HPP_
#define ACROBOTDYNAMICS_HPP_

#include <cmath>
#include <Eigen/Dense>
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>

/* acrobot dynamics */

class acrobotDynamics: public DynamicsBase<acrobot::acrobotDimensions> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

	acrobotDynamics() {}

	typedef typename acrobot::acrobotDimensions::state_vector_t state_vector_t;
	typedef typename acrobot::acrobotDimensions::control_vector_t control_vector_t;
	typedef typename acrobot::acrobotDimensions::state_matrix_t state_matrix_t;
	typedef typename acrobot::acrobotDimensions::control_gain_matrix_t control_gain_matrix_t;

	void setRigidBodyDynamics(const state_vector_t &x);

	acrobot::acrobotDimensions::state_vector_t systemDynamics(const state_vector_t & x, const control_vector_t & u);

private:

	Eigen::Matrix2d M;
	Eigen::Vector2d H;
	Eigen::Vector2d PHI;
	Eigen::Vector2d DAMP;

};

acrobot::acrobotDimensions::state_vector_t acrobotDynamics::systemDynamics(const state_vector_t & x, const control_vector_t & u) {

	// Code the Equation of motion, the function receives the states and requires the derivatives back

	acrobot::acrobotDimensions::state_vector_t dxdt;

	setRigidBodyDynamics(x);

	Eigen::Vector2d input_torque;
	Eigen::Vector2d qdd;

	input_torque.setZero();
	input_torque(1) = u(0);

	qdd = M.inverse() * ( input_torque - H - PHI - DAMP);

	dxdt.segment<2>(2) = qdd;
	dxdt.segment<2>(0) = x.segment<2>(2);

	return(dxdt);
}

void acrobotDynamics::setRigidBodyDynamics(const state_vector_t & x)
{

	//ToDo : These parameters can be read from the URDF file!

	M.setZero();
	H.setZero();
	PHI.setZero();

	/* constants */

	double g = 9.81;

	double m2 = 1.0;
	double lc2 = 1.0;
	double Ic2 = 0.333;
	double b1 = 0.1;

	double m1 = 1.0;
	double l1 = 1.0;
	double lc1 = 0.5;
	double Ic1 = 0.083;
	double b2 = 0.1;

	double I1 = Ic1 + m1 * pow(lc1,2);
	double I2 = Ic2 + m2 * pow(lc2,2);

	double q1 = x(0);
	double qd1 = x(2);

	double q2 = x(1);
	double qd2 = x(3);

  double aux = m2 * l1 * lc2;

  //double c1 = cos(q1);
  double c2 = cos(q2);
  double s1 = sin(q1); double s2 = sin(q2);
  double s12 = sin(q1+q2);

  double d12 = I2 + aux * c2;
  double d11 = I1 + I2 + m2 * pow(l1,2) + 2 * aux * c2;
  double d21 = d12;

  double d22 = I2;
  double h11 = -2 * aux * s2 * qd2;
  double h12 = -aux*s2*qd2 ;
  double h21 = aux * s2 * qd1;

  double h22 = 0;

  Eigen::Matrix2d C;

  C << h11, h12 , h21 , h22;

  double phi1 = g * (  m1 * lc1 * s1 + m2*(l1 * s1 + lc2 * s12) );
  double phi2 = g * m2 * lc2 * s12;

	double damp1 = b1*qd1;
	double damp2 = b2*qd2;

	M << d11 , d12 , d21 , d22;
	H  = C * x.segment<2>(2);
	PHI << phi1 , phi2;
	DAMP << damp1 , damp2;

}
#endif /* ACROBOTDYNAMICS_HPP_ */
