/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file DynamicsBase.hpp
 *  \brief Base Class defining the dynamics of a system
 *  \date Based on the original file created on: 26.03.2014
 *  \authors neunertm, dpardo
 */

/*! \mainpage Dynamical Systems Package
 *  \brief A package defining kinematic and dynamic models for nonlinear systems.
 *
 * \section Overview
 *
 * This package provides Base classes to define the dynamics and kinematics of
 * nonlinear systems.
 *
 * It provides the interfaces required to apply the "Direct Trajectory Optimization" package
 * to a system.
 *
 * \subsection References
 *
 * - The main class DIMENSIONS specifies all the features of a system
 * - The base class DerivativesNumDiffBase generates the derivatives of a system with respect to x and u.
 * - The base class LeggedRobotDynamics provides all the interfaces required to use the DTO package
 *
 *
 */

#ifndef DYNAMICSBASE_HPP_
#define DYNAMICSBASE_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>

/**
 * @brief Base class to provide the equation of motion of a system
 * @tparam DIMENSIONS class defining all types according to the features of the system
 */
template <class DIMENSIONS>
class DynamicsBase {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;
	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_gain_matrix_t control_gain_matrix_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
	typedef typename DIMENSIONS::control_feedback_array_t control_feedback_array_t;


	DynamicsBase() {}
	virtual ~DynamicsBase() {}

	virtual state_vector_t systemDynamics( const state_vector_t & x , const control_vector_t & u) = 0;

};


#endif /* DYNAMICSBASE_DS_HPP_ */
