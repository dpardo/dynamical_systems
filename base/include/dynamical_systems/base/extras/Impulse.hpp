/*
 * Impulse.hpp
 *
 *  Created on: Jul 24, 2017
 *      Author: depardo
 */

#ifndef _IMPULSE_HPP_
#define _IMPULSE_HPP_

#include <Eigen/Dense>
#include <memory>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>

template <class DIMENSIONS>
class Impulse {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
  typedef typename DIMENSIONS::state_vector_t state_vector_t;
  typedef typename DIMENSIONS::control_vector_t control_vector_t;
  typedef typename DIMENSIONS::EELambda_t EELambda_t;
  typedef typename DIMENSIONS::InertiaMatrix_t InertiaMatrix_t;
  typedef typename DIMENSIONS::EEJacobianTranspose_t EEJacobianTranspose_t;

  Impulse(std::unique_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd):lrd_(std::move(lrd)) {

    fc_ = lrd_->getContactConfiguration();
    is_valid_ = lrd_->grf_->isValid();
  }

  void changeContactConfiguration(const int & c_code){
    lrd_->setContactConfiguration(c_code);
    fc_ = lrd_->getContactConfiguration();
    is_valid_ = lrd_->grf_->isValid();
  }
  void changeContactConfiguration(const ContactConfiguration_t & fc) {
    fc_ = fc;
    lrd_->setContactConfiguration(fc);
    is_valid_ = lrd_->grf_->isValid();
  }

  EELambda_t updateImpulse(const state_vector_t & x1 , const state_vector_t & x2);

  std::unique_ptr<LeggedRobotDynamics<DIMENSIONS>> lrd_;
  EELambda_t impulse_vector_;
  ContactConfiguration_t fc_;
  bool is_valid_ = true;

};


template <class DIMENSIONS>
typename DIMENSIONS::EELambda_t Impulse<DIMENSIONS>::updateImpulse(const state_vector_t & x1,
                                                                   const state_vector_t & x2) {

//  std::cout << "This vector should be zero : " << std::endl;
//  std::cout << (x2-x1).transpose() << std::endl;

  InertiaMatrix_t P,M;
  EEJacobianTranspose_t Jinv;
  GeneralizedCoordinates_t q1,q2,qd1,qd2,b;
  EELambda_t impulse_base;
  impulse_vector_.setZero();

  lrd_->getCoordinatesFromState(x1,q1,qd1);
  lrd_->getCoordinatesFromState(x2,q2,qd2);

  lrd_->updateState(x2);
  lrd_->getOrthogonalProjection(P,Jinv);

  lrd_->getInertiaMatrix(q2,M);

  b = M*(qd2-qd1);
  impulse_base = Jinv.transpose()*b;

  std::cout << "-----" << std::endl;
  std::cout << Jinv.transpose() << std::endl;
  std::cout << "-----" << std::endl;
  std::cout << lrd_->getJc() << std::endl;
  std::cout << "-----" << std::endl;

  std::vector<int> legs_id = lrd_->getContactPointIDVector();

  // rotate!

  for(int ee = 0 ; ee < lrd_->getNumberPointsContact() ; ee++) {
      Eigen::Affine3d A = lrd_->getWXBTransform();
      Eigen::Vector3d imp = A.linear()*impulse_base.template segment<3>(legs_id[ee]*3);
      impulse_vector_.template segment<3>(legs_id[ee]*3) = imp;
  }

  return impulse_vector_;

}
#endif /* _IMPULSE_HPP_ */
