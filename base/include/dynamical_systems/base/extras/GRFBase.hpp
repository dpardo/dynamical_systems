/*
 * GRFBase.hpp
 *
 *  Created on: Apr 24, 2017
 *      Author: depardo
 */

#ifndef _GRF_GRFBASE_HPP_
#define _GRF_GRFBASE_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/Dimensions.hpp>

template <class DIMENSIONS>
class GRFBase {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::EELambda_t EELambda_t;
  typedef typename DIMENSIONS::EEForceDataMap_t EEForceDataMap_t;
  typedef typename DIMENSIONS::EELambda_single_component_t EELambda_single_component_t;
  typedef typename DIMENSIONS::EEFriction_cone_coefficient_t EEFriction_cone_coefficient_t;
  typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
  static const int kTotalEE = DIMENSIONS::kTotalEE;

  GRFBase(const ContactConfiguration_t & fc):feet_configuration_(fc) {}
  GRFBase():feet_configuration_(false){};
  virtual ~GRFBase(){};

  void setContactForces(const EELambda_t & cf);
  void clear();
  bool isValid(){return contact_forces_valid_flag_;}
  bool validateContactForces();

  bool changeFeetConfiguration(const ContactConfiguration_t & fc);

  virtual void getZComponent(EELambda_single_component_t & Fz);
  virtual void getContactForcesVector(EELambda_t & Lambda );
  virtual EELambda_t  getContactForcesVector(){
    return contact_forces_vector_;
  }
  virtual void getFrictionConeConstraint(const double & mu , EEFriction_cone_coefficient_t & gma);

//protected:

  ContactConfiguration_t feet_configuration_;
  EEForceDataMap_t contact_forces_data_map_;
  EELambda_t contact_forces_vector_;
  bool contact_forces_valid_flag_ = false;

};

template <class DIMENSIONS>
void GRFBase<DIMENSIONS>::setContactForces(const EELambda_t & cf) {

  //ToDo : Optimize this code
  contact_forces_vector_ = cf;

  for(int leg = 0 ; leg < kTotalEE; ++leg ){
      int index = leg*3;
      contact_forces_data_map_[leg] = contact_forces_vector_.segment(index,3);
  }
}

template <class DIMENSIONS>
void GRFBase<DIMENSIONS>::clear() {
  setContactForces(EELambda_t::Zero());
}

template <class DIMENSIONS>
bool GRFBase<DIMENSIONS>::changeFeetConfiguration(const ContactConfiguration_t & fc) {

  feet_configuration_=fc;
  return(validateContactForces());
}

template <class DIMENSIONS>
bool GRFBase<DIMENSIONS>::validateContactForces() {

  int test = 0;
  for(int k = 0 ; k < kTotalEE ; ++k) {
    if(feet_configuration_[k])
      test++;
  }

  if(test == 0)
    contact_forces_valid_flag_ = false;
  else
    contact_forces_valid_flag_ = true;

  return contact_forces_valid_flag_;
}

template <class DIMENSIONS>
void GRFBase<DIMENSIONS>::getZComponent(EELambda_single_component_t & Fz) {

  for(int leg = 0 ; leg < kTotalEE ; ++leg) {

      Fz(leg) = contact_forces_data_map_[leg](2);
  }
}

template <class DIMENSIONS>
void GRFBase<DIMENSIONS>::getContactForcesVector(EELambda_t & Lambda) {

  Lambda = contact_forces_vector_;

}

template <class DIMENSIONS>
void GRFBase<DIMENSIONS>::getFrictionConeConstraint(const double & mu, EEFriction_cone_coefficient_t & fcc) {

  for(int leg = 0 ; leg < kTotalEE ; leg++) {
      Eigen::Vector3d legforce = contact_forces_vector_.template segment<3>(3*leg);
      fcc[leg] = legforce.segment<2>(0).norm() - mu*legforce(2);
  }

}

#endif /* _GRF_GRFBASE_HPP_ */
