/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * EE_data_map.hpp
 *
 *  Created on: Aprl 24
 *      Author: Diego Pardo
 *      Based on the original idea and version from IIT
 *      IIT HYQ COMMONS LEGDATAMAP
 *      This class generalizes such concept to any robot and End Effector
 */

#ifndef _EEDATAMAP_H_
#define _EEDATAMAP_H_

#include <dynamical_systems/tools/data_map.h>

template<typename T, size_t N>
class EEDataMap : public DataMap<T, N> {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  enum EEId {
    EE_1=0,
    EE_2,
    EE_3,
    EE_4,
    EE_5,
    EE_6,
    EE_7,
    EE_8,
  };

  EEDataMap():DataMap<T,N>(){}
  EEDataMap(const T & value):DataMap<T,N>(value){}
  EEDataMap(const T* value):DataMap<T,N>(value){}
  EEDataMap(const EEDataMap<T,N> & data_map):DataMap<T,N>(data_map){}
 // EEDataMap(const T value):DataMap<T,N>(value){}

};

//template <typename T, size_t N>
//using EEDataMap_t = DataMap<T,  N>;

#endif // _EEDATAMAP_H_
